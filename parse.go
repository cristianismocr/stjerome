package main

import (
	"io"
	"io/ioutil"
	"regexp"

	"gopkg.in/neurosnap/sentences.v1"
)

const readSize = 512

// Oracion contiene una cadena de texto cuyo contenido se extiende hasta el
// primer punto que encuentra.
type Oracion struct {
	Orden     int
	Contenido []byte
}

// Parrafo es una lista de oraciones que termina hasta encontrar un doble salto
// de linea
type Parrafo struct {
	Orden     int
	Oraciones []Oracion
}

// ParseParrafos procesa un corpus de texto y devuelve una lista de parrafos
func ParseParrafos(i io.Reader) ([]Parrafo, error) {
	b, err := ioutil.ReadFile("data/spanish.json")
	if err != nil {
		return nil, err
	}
	training, err := sentences.LoadTraining(b)
	if err != nil {
		return nil, err
	}
	// crea tokenizador
	tokenizer := sentences.NewSentenceTokenizer(training)

	chunk := make([]byte, readSize)
	var buff []byte
	for {
		_, err := i.Read(chunk)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
	}

	var parrafos []Parrafo
	pattern := regexp.MustCompile(`(?m)\n{2,}`)
	trozos := pattern.Split(string(buff[:]), -1)
	for pIndex, parrafo := range trozos {
		oraciones := tokenizer.Tokenize(parrafo)
		p := Parrafo{Orden: pIndex + 1}
		for oIndex, oracion := range oraciones {
			o := Oracion{Orden: oIndex + 1, Contenido: []byte(oracion.Text)}
			p.Oraciones = append(p.Oraciones, o)
		}
		parrafos = append(parrafos, p)
	}
	return parrafos, nil
}
