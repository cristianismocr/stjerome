package main

import (
	"bytes"
	"testing"
)

func TestParseParrafos(t *testing.T) {
	table := []struct {
		corpus   []byte
		parrafos int
	}{
		{
			corpus: []byte(`Hola mundo!

Esto es una prueba. Una prueba sencilla, la verdad
Aunque no hay que dejarse engañar por lo sencillo que se ve.`),
			parrafos: 2,
		},
		{
			corpus: []byte(`\\libro{prueba}

Esto es una prueba \\footnote{nota pequeña. Al pie de pagina. Hola mundo} 
pequeña para ver como reacciona esto.`),
			parrafos: 2,
		},
	}
	for _, inputs := range table {
		reader := bytes.NewReader(inputs.corpus)

		result, err := ParseParrafos(reader)
		if err != nil {
			t.Errorf("fallo por error: %v", err)
		}
		if result == nil {
			t.Errorf("no se devolvió ningún párrafo")
		}
		if len(result) > inputs.parrafos {
			t.Errorf("la cantidad de párrafos devueltos excede lo esperado: %d", len(result))
		}
	}
}
